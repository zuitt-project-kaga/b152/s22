# Start data handling

	Type : Course Booking System
	Description : Student/User should be able to book into a course.
	Features :
        -user registration
        -user authentication/login
            -Features Regular Logged In Users:
                -view available courses
                -enroll in a course
                -update their own details
                -delete their optional details (except for
                credentials)
            -Feature Admin User
                -create courses
                -update courses
                -archive/deactivate courses
                -reactivate courses
                -view all courses (active/inactive)

    Models : 
		users
            {
                (id) -unique identif

            }
		courses


# Data Modelling

    Client : E-commerce system
    Type : e-commerce system
    Description : User/Shoppers can create Orders.Each Order can only refer to a single shopper. Each order can contain multiple products.
    Feature :
        -user registration
        -user authentication/login
            Regular User feature:
                -create orders


{ 
    "users_1": {
        "id" : "01",
        "course" : "Science"
    } ,
    "users_2" :{
        "id" : "02",
        "course" : "Math"
    }
},
{
    "course_1" : {

    }
}
